FROM registry.gitlab.com/xxllnc/zaakgericht/zaken/libraries/zsnl-p5-bttw_tools:v0-014

RUN cpanm LWP::Protocol::https \
 && cpanm https://gitlab.com/xxllnc/zaakgericht/zaken/libraries/zsnl-p5-syzygy.git@v0.004 \
 && cpanm https://gitlab.com/xxllnc/zaakgericht/zaken/libraries/zsnl-p5-instance_config.git \
 && rm -rf /root/.cpanm \
 || (cat /root/.cpanm/work/*/build.log && false)

CMD ["/bin/bash"]
